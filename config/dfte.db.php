<?php 	
include ("dfte.config.php");
try {
	$dsn = 'mysql:dbname=' . DB_NAME . ';host=' . DB_HOST;
    $db_conn = new PDO($dsn, DB_USER, DB_PASS);
} 
catch (PDOException $e) {    
    echo "Could not connect to database " . DB_NAME . "\n";
    echo $e;
    exit(1);
}

?>