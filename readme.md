# Digital Forensic Tools Catalogue Editor

## An Editor fot the Catalogue of Forensic Tools covering a wide range of Categories, from Mobile to Computer Forensic


# Editor Structure
The Editor has been structured in three main parts:

* Login
* Editing: Updating and Adding new tools
* Approval

## Login
The editing of the Catalogue is allowed only to registered users. The registration is free but it will be evaluated by the Catalogue Administrators and only after their approval it will be possible to enter the Editor environment.
After the registration, the new user will see a message explaining the approval process: his/her request is sent, by email, to the Administrators and if they will approve the request they will receive the confirmation and from that point on they will gain access to the Catalogue, through their credentials, and could provide their contribution to its updating and widening.

## Editing
The Editing is carried out in two ways:

__UPDATING__ The process consists of preparing a query over the Catalogue for searching a group of tools. This facility is provided in a similar way to the Catalogue browsing interface. After having extracted the list of tools, it will be possible to edit a specific tool, activating the corresponding link on the icon  put next to the toll name.
When, next to the tool name, there is the icon  the tools is not editable because it has already modified by another user and the changing proposal is still pending, that is the Administrators have not approved it yet.
If the link is active and it is triggered, it opens the editing form showing all the fields filled with the related tool's values. The Editing process also allows the assignment of a different Category/Features to the tool current value. Nevertheless the program carries out a coherent check: if the tool has already some Features in the selected Category the assignment will be banned because for this updating it will be mandatory to search the Tool in that Category and select it for the editing. The fields Test Reports and Useful References are defined as multiple values: each single tool may have more than one value. Each value is formed by a couple of data: an Url address and a Note. Each value can be added to the list using the Url and Note edit text and the Button
It is also possible select a value (Url + Note) from the list for its editing or for cancel it by using the button

__NEW TOOL__ To introduce a new tool in the Catalogue the "+ Tool button  must be used. When a user press the New Tool button the system opens the editing form showing all fields set to blank. The editing mode is similar to the previous UPDATING process.
Each time an user introduces an amendment (updating or widening) the Administrator will receive an email about the operation carried out.

__APPROVAL__
The approval process can be carried out only by users who play the role of Administrators. For these users in the top left corner the Approval link will be visible. Activating this link the system shows a list of pending updating, using the icons:
 * Tool in update status
 * Tool in adding status

The user Administrator can approve a single modification, through one of the above buttons, or use the  button to confirm all the pending amendments.
The user Administrator can also refuse an amendments using the  button including the motivation of his/her decision in the provided text field, that explanation will be sent to the editor by email After the approval/refusal process, each user will receive an email of the accepted/refused amendments suggested. The user Administrator can approve a single modification or use the Approve All button to confirm all the pending amendments.
The increasing complexity of today’s digital forensics analyst activities depends on a multiple of elements that include, but are not limited to the following:
The increasing number of devices; the different operating systems; the huge number of applications, in particular devoted to mobile devices the different file formats to interpret and process.

The effectiveness of a forensic investigation depends mainly on the forensic analyst’s expertise and experience but also on the availability of forensic tools being able to extract and interpret data (potential evidence) quickly.
