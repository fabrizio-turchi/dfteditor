function Sort(field, direction) {
    document.frmResults.direction.value = direction;
    document.frmResults.sort.value = field;
    document.frmResults.submit();    
}

function OpenWin(url) {
    var options = "width=500, height=300, scrollbars, alwaysRaised";
    var newWindow = window.open(url, "WebTool", options);
}

function ViewToolOs(value) {
    fOsSearch = parent.search.document.frmSearch.os;
    
    document.frmResults.os.value = value;

    document.frmResults.qryCatalogue.value = ''; // qryCatalogue  is used only for sorting previous results
    document.frmResults.sort.value = "Tool";
    document.frmResults.direction.value= "ASC";
    
    nOs = fOsSearch.length;
    for(i=0; i <nOs; i++) {
        if (fOsSearch.options[i].value == value) {
            fOsSearch.selectedIndex = i;
            break;
        }            
    }
    document.frmResults.submit();
}

function ViewToolCategory(code) {
    fCode = document.frmResults.formCategories;
    fCategory = parent.search.document.frmSearch.CodeCategory;
    
    for (i=0; i < fCode.length; i++) {
        if (fCode.options[i].value == code) {
            document.frmResults.CodeCategory.value = code;
            document.frmResults.Category.value = fCode.options[i].text;
            break;            
        }
    }
    
    for (i=0; i<fCategory.length; i++) {
        if (fCategory.options[i].value == code) {
            fCategory.selectedIndex = i;
            break;
        }            
    }        
    
    window.parent.search.CheckCategory();
    
    document.frmResults.qryCatalogue.value = ''; // qryCatalogue  is used only for sorting previous results
    document.frmResults.sort.value = "Tool";
    document.frmResults.direction.value= "ASC";
        
    document.frmResults.submit();                 
}    

function ViewToolLicense(value) {
    fLicenseSearch = parent.search.document.frmSearch.license;
    
    document.frmResults.license.value = value;

    document.frmResults.qryCatalogue.value = ''; // qryCatalogue  is used only for sorting previous results
    document.frmResults.sort.value = "Tool";
    document.frmResults.direction.value= "ASC";
    
    nLicenses = fLicenseSearch.length;
    for(i=0; i <nLicenses; i++) {
        if (fLicenseSearch.options[i].value == value) {
            fLicenseSearch.selectedIndex = i;
            break;
        }            
    }
    document.frmResults.submit();
}