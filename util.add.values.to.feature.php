<?php
/*
*	util.add.values.to.feature
*
*	Add one or more Values, separated by commma, to an existing Feature.
*	It is a module to run in the command line with two parameters:
*	
*	IdFeature (from tblFeatures)
*	Values (list of strings separatede by comma)
*
*	For instance the Category "Database File" under "File analisys" has the
*	Feature "DB format" that in the tblFeatures table has the value of 85. 
*	At the moment the Feature has the values "DB2", "ESE DB", "MS SQL Server"
*	"MySQL", "Oracle", "Others", "PostgreSQL", "SQLite".
*	If I want to add the new Value "Firebird" I run the utility ad it follows:
*
*		util.add.value.to.feature 83 Firebird
*
*/
	
	if (PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) {
         echo "Operation not permitted";
         exit(1);
    }
    include "config/dfte.db.php";

	$arrayValues = explode(",", $argv[2]);			// Values are separated by comma
	
	foreach ($arrayValues as $value) {
		try { 
			$qryInsert  = "INSERT INTO tblFeaturesValues (IdFeature, Value) VALUES (";
	    	$qryInsert .= ":IdFeature, :Value) ";
			
			$stmt = $db_conn->prepare($qryInsert);
			$stmt->bindParam(':IdFeature', $argv[1], PDO::PARAM_INT);
			$stmt->bindParam(':Value', $value, PDO::PARAM_STR);
			$stmt->execute();
		}
		catch (Exception $e) {
			echo "ERROR in Insert FeaturesValues";
		   	echo $e->getMessage();
		   	exit;
		}
	}
	echo "Insert accomplished!\n\n";
?>
