<?php   
/*
*---------- 
*       util.generate.features.values.box.size.php
*----------
*       generate the file dftc.boxsize.php to determine the size of the 
*       dynamic panel containing the Features and Values for each Category.
*       The file must be generated every time a Category, a Feature or one of
*       its Values change. The dftc.boxsize.php file is included in the
*       module dftc.search.php
*/
    if (PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) {
         echo "Operation not permitted";
         exit(1);
    }

    define("FILE_OUT", "dfte.boxsize.php");
    define("SPACE_FEATURE", 30);        // space taken by each check box of the  Feature in the Panel
    define("SPACE_VALUE", 25);          // space taken by each couple of Values on the Features Panel
    define("SPACE_HR", 25);             // space taken by <hr> for Features separator   
    define("SIZE_BASE", 50);            // default size Features Panel

    include "config/dfte.db.php";
    
    try{
        $fEditor = fopen(FILE_OUT, "w");        
    }
    catch (Exception $e) {
        echo "Could not open the Editor file";
        exit; 
    }

      
    fwrite($fEditor, "<?php \n");
    echo "\n";
    generateBox($fEditor, "AN");
    echo "\n";
    generateBox($fEditor, "AC");
    fwrite($fEditor, "?>\n");

    fclose($fEditor);
?>    

<?php 
/*
*  generateBox(): generate the size of the Features box on the basis of the selected Category in the Caegory combo box
*
*                   $fOut: handle output file
*                   $process: it assumes AN (Analysis) or AC (Acquisiton) 
*
*/    
function generateBox($fEdit, $process) {
    global $db_conn;

    $qryCategories  = "SELECT CodeCategory, Category FROM tblCategories WHERE Process='" . $process . "' ";
    $qryCategories .= "ORDER BY CodeCategory";
    
    $rsCategories   = $db_conn->query($qryCategories);
    $nCategories    = $rsCategories->rowCount(); 
    fwrite($fEdit, "echo '<select class=dftHidden name=boxSizes" . $process . ">';\n");
    fwrite($fEdit, "echo '<option value=" . SIZE_BASE . ">0</option>';\n");
    
    for($i=0; $i<$nCategories; $i++) {
        $rowCategory = $rsCategories->fetch();
        $qryFeatures  = "SELECT IdFeature FROM tblFeatures WHERE CodeCategory='";
        $qryFeatures .= $rowCategory["CodeCategory"] . "' AND Process='" .$process . "' ";
        $rsFeatures   = $db_conn->query($qryFeatures);
        $nFeatures    = $rsFeatures->rowCount(); 
        $nValues = 0;

        for($j=0; $j<$nFeatures; $j++) {
            $rowFeature = $rsFeatures->fetch();
            $qryValues = "SELECT COUNT(IdFeatureValue) AS numValues FROM tblFeaturesValues WHERE IdFeature=" . $rowFeature["IdFeature"];
            $rsValues  = $db_conn->query($qryValues);
            $rowValues = $rsValues->fetch();
            $nValue    = intval($rowValues["numValues"]);

            $nValues += SPACE_FEATURE   ;                                                  // space taken by the Feature
            $nValues += (intval($nValue / 2) + intval($nValue) % 2) * SPACE_VALUE;  // space taken by Values
        }
    
        $nValues += ($nFeatures - 1) * SPACE_HR;         

        //*debug echo $rowCategory["CodeCategory"] . " - nF=" . $nFeatures . " - nV=" . $nValues .  "\n";

        fwrite($fEdit, "echo '<option value=" . $nValues . ">" . $rowCategory["CodeCategory"] . "</option>';\n");
    }
    fwrite($fEdit, "echo '</select>';\n");
    echo "File dftc.boxsize" . $process . ".php generated \n";
}
?>
